/*
* ******************************** *
* ----------- START UP ----------- *
* ******************************** *
*/

$(document).ready(function(){
  
  // ----------- Generate data -----------
  
  $('#generate').click(function(event){
    event.preventDefault();
    showAndHide($('#loader'),$('#patient-chooser'));
    $('#list').html('');
    for(var i = 1; i <= 3; i++)
      generirajPodatke(i);
    showAndHide($('#patient-chooser'),$('#loader'));
  });
  
  // ----------- Init Google Map ----------
  $('.building').click(function (event) {
    event.preventDefault();
    $('#map').removeClass('hide');
    var selectedItem = $(this).find('a').attr('href');
    initMap(selectedItem);
  });
  
  // ----------- Choose patient -----------
  $(document).on('click','#list a', function(event){
    event.preventDefault();
    var ehrId = $(this).attr('ehr-id');
    $('#input-ehrid').val(ehrId);
    $('#get-data').click();
  });

  // ------------- Get data after selectiong patient -------------
  $('#get-data').click(function(event){
    event.preventDefault();
    var ehrId = $('#input-ehrid').val();
    $('.nav-pills a[href="#tab-ideal"]').tab('show');
    $('#map').addClass('hide');
    
    getData(ehrId, function(data){
      showAndHide($('#patient-record'), $('#error'));
      
      // Setting variables
      var firstName = data.party.firstNames;
      var lastName = data.party.lastNames;
      var dateOfBirth = data.party.dateOfBirth;
      var currentAge = calcAge(dateOfBirth);
      
      var currentHeight = data.height[0].height;
      var currentWeight = data.weight[0].weight;
      var currentSbp = data.bp[0].systolic;
      var currentDbp = data.bp[0].diastolic;
      var currentSpO2 = data.spO2[0].spO2;
      
      // Set general info
      $('#fullname').text(firstName + ' ' + lastName);
      $('#date-of-birth').text(dateOfBirth);
      $('#last-height').text(currentHeight + ' cm');
      $('#last-weight').text(currentWeight + ' kg');
      $('#last-bp').text(currentSbp + '/' + currentDbp + ' mm Hg');
      $('#last-sbp-progress').text(currentSbp + 'mm Hg');
      $('#last-dbp-progress').text(currentDbp + 'mm Hg');
      $('#last-spO2-progress').text(currentSpO2 + ' %');
      
      // Set progress bars
      $('#last-spO2-progress').width(currentSpO2 + '%');
      $('#last-sbp-progress').width((currentSbp)/(220)*100 + '%');
      $('#last-dbp-progress').width((currentDbp)/(140)*100 + '%');
      
      // Set table headers
      $('#table-bmi').html('<thead><tr><th>Datum in ura</th><th>Višina</th><th>Masa</th><th>ITM</th><th>Stanje</th></tr></thead>');
      $('#table-bmr').html('<thead><tr><th>Datum in ura</th><th>Starost</th><th>Višina</th><th>Masa</th><th>BM</th></tr></thead>');

      var graphData = [];
      for(var i = 0; i < data.weight.length; i++){
        
        var date = data.weight[i].time;
        var weight = data.weight[i].weight;
        var height = data.height[i].height;

        var bmiData = BMI(height, weight);
        var bmrData = BMR('male', height, weight, currentAge, 1);
        var idealWeight = calcIdealWeight('male', height, weight);

        $bmiRow = $('<tr><td>' + date + '</td><td>' + bmiData.height + '</td><td>' + bmiData.weight + '</td><td>' + bmiData.currentBMI.toFixed(0) + '</td><td>' + bmiData.statusBMI + '</td></tr>');
        $('#table-bmi').append($bmiRow);

        $bmrRow = $('<tr><td>' + date + '</td><td>' + bmrData.age + '</td><td>' + bmrData.height + '</td><td>' + bmrData.weight + '</td><td>' + bmrData.currentBMR.toFixed(0) + '</td></tr>');
        $('#table-bmr').append($bmrRow);

        var date = new Date(date);
        var year = date.getFullYear(), month = date.getMonth(), day = date.getDate();
        day = (day < 10)?'0' + day:day;
        month = (month < 10)?'0' + month:month;
        var tmpGraphData = {"date": year+'-'+month+'-'+day,"Vaša masa": weight, "Idealna masa": idealWeight.idealWeight};
        graphData.push(tmpGraphData);
      }

      drawGraph(graphData);
    });
  });
});

/*
* ********************************************** *
* ----------------- FUNCTIONS ------------------ *
* ********************************************** *
*/

var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

/*
*
* ----------- GENERATE PATIENTS -----------
*
*/ 

function generirajPodatke(stPacienta) {
  stPacienta--;
  var patient_data;
  $.ajax({
      url: 'res/patients/pacienti.json',
      dataType: 'json',
      success: function(data) {
        patient_data = data;
      }
    });

  var sessionId = getSessionId();
  var ehrId;
  var nurse = 'dr. med. Gospa Zdravnica';

  $.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	$.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    success: function (data) {
      ehrId = data.ehrId;
      var params = {
          ehrId: ehrId,
          templateId: 'Vital Signs',
          format: 'FLAT',
          committer: nurse
      };
      var patient_data_1 = {
          firstNames: patient_data[stPacienta].firstName,
          lastNames: patient_data[stPacienta].lastName,
          dateOfBirth: patient_data[stPacienta].dateOfBirth,
          partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
      };

      $.ajax({
        url: baseUrl + "/demographics/party",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(patient_data_1),
        success: function (data1) {
          if (data1.action == 'CREATE') {
            for(var i = 0; i < 4; i++)
              postData(i, function(){});
            postData(4, function(json){
              fillDropdown([json]);
            });

            function postData(i, callback){
              var patient_data_2 = {
                  "ctx/language": "en",
                  "ctx/territory": "SI",
                  "ctx/time": patient_data[stPacienta].vitalSigns[i].date,
                  "vital_signs/height_length/any_event/body_height_length": patient_data[stPacienta].vitalSigns[i].height,
                  "vital_signs/body_weight/any_event/body_weight": patient_data[stPacienta].vitalSigns[i].weight,
                  "vital_signs/body_temperature/any_event/temperature|magnitude": patient_data[stPacienta].vitalSigns[i].temperature,
                  "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                  "vital_signs/blood_pressure/any_event/systolic": patient_data[stPacienta].vitalSigns[i].bloodPressureSystolic,
                  "vital_signs/blood_pressure/any_event/diastolic": patient_data[stPacienta].vitalSigns[i].bloodPressureDiastolic,
                  "vital_signs/indirect_oximetry:0/spo2|numerator": patient_data[stPacienta].vitalSigns[i].oxygenSaturation
              };
              $.ajax({
                url: baseUrl + "/composition?" + $.param(params),
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(patient_data_2),
                success: function (data1) {
                  var json = {
                    firstName: patient_data[stPacienta].firstName,
                    lastName: patient_data[stPacienta].lastName,
                    ehrId: ehrId
                  };
                  callback(json);
                },
                error: function(err) {
                  showAndHide($('#error'), $('#patient-record'));
                  console.error(err);
                }
              });
            }
          }
        },
        error: function(err) {
          showAndHide($('#error'), $('#patient-record'));
          console.error(err);
        }
      });
    },
    error: function(err){
      showAndHide($('#error'), $('#patient-record'));
      console.error(err);
    }
  });
  return ehrId;
}

/*
* -------------------- GET PATIENT DATA ---------------------
*
* Function returns data about certain patient that is collected from EHRSpace over their api with ehrID
*/

function getData(ehrId, callback){
  var json;
  var sessionId = getSessionId();
  $.ajax({
		url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
  	type: 'GET',
  	headers: {"Ehr-Session": sessionId},
  	success: function (data1) {
			$.ajax({
		    url: baseUrl + "/view/" + ehrId + "/weight",
		    type: 'GET',
		    headers: {"Ehr-Session": sessionId},
		    success: function (data2) {
          $.ajax({
    		    url: baseUrl + "/view/" + ehrId + "/height",
    		    type: 'GET',
    		    headers: {"Ehr-Session": sessionId},
    		    success: function (data3) {
              $.ajax({
        		    url: baseUrl + "/view/" + ehrId + "/blood_pressure",
        		    type: 'GET',
        		    headers: {"Ehr-Session": sessionId},
        		    success: function (data4) {
                  $.ajax({
                	   url: baseUrl + "/view/" + ehrId + "/spO2",
                	   type: 'GET',
                	   headers: {"Ehr-Session": sessionId},
                	   success: function (data5) {
                      json = {
                          party: data1.party,
                          weight: data2,
                          height: data3,
                          bp: data4,
                          spO2: data5
                        };
                        callback(json);
                	   },
                    error: function(err){
                      showAndHide($('#error'), $('#patient-record'));
                      console.error(err);
                    }
                  }); 
            		},
                error: function(err){
                  showAndHide($('#error'), $('#patient-record'));
                  console.error(err);
                }
        			});
    		    },
            error: function(err){
              showAndHide($('#error'), $('#patient-record'));
              console.error(err);
            }
    			});
		    },
        error: function(err){
          showAndHide($('#error'), $('#patient-record'));
          console.error(err);
        }
			});
    },
    error: function(err){
      showAndHide($('#error'), $('#patient-record'));
      console.error(err);
    }
	});
}


/*
*
* ----------- CALCULATIONS -----------
*
*/

function calcAge(date) {
    var now = new Date(date);
    var millis = Date.now() - now.getTime();
    var dif = new Date(millis);
    return Math.abs(dif.getUTCFullYear() - 1970); // 1970.01.01 --> Same in Java
}

function calcIdealWeight(sex, height, weight){
  var variable = (sex=='male')?88:92;

  var idealWeight = 0.9 * height - variable;

  var json = {
    idealWeight: idealWeight
  };

  return json;
}

function BMI(height, weight){
  var currentBMI = weight / ((height/100) * (height/100));

  var statusBMI;
  if(currentBMI <= 18.5)
    statusBMI = "podhranjenost";
  else if(currentBMI > 18.5 && currentBMI <= 24.9)
    statusBMI = "normalno";
  else if(currentBMI > 25 && currentBMI <= 29.9)
    statusBMI = "prekomerna teža";
  else
    statusBMI = "debelost"

  var json = {
    height: height,
    weight: weight,
    currentBMI: currentBMI,
    statusBMI: statusBMI,
  };

  return json;
}

function BMR(sex, height, weight, age, multiplier){
  var variable = (sex == 'male')?[66, 13.7, 5, 6.8]:[655, 9.6, 1.8, 4.7];

  var currentBMR = (variable[0] + (variable[1] * weight) + (variable[2] * height) - (variable[3] * age)) * multiplier;

  var json = {
    height: height,
    weight: weight,
    age: age,
    currentBMR: currentBMR
  };

  return json;
}

/*
*
* ----------- MISC -----------
*
*/

function fillDropdown(list){
  console.log(list[0]);
  $list = $('<li><a ehr-id="' + list[0].ehrId + '" href="#">' + list[0].firstName +  ' ' + list[0].lastName +  '</a></li>');
  $('#list').append($list);
}

function showAndHide($show, $hide){
  $hide.addClass('hide');
  $show.removeClass('hide');
}

/*
*
* ----------- GRAPH -----------
*
*/

function drawGraph(data){
  $graphContainer = $('#graph');
  var graphWidth = $graphContainer.width();
  $graphContainer.html('');

  var margin = {top: 20, right: 40, bottom: 30, left: 40},
      width = graphWidth - margin.left - margin.right,
      height = 500 - margin.top - margin.bottom;

  var parseDate = d3.time.format("%Y-%m-%d").parse;

  var x = d3.time.scale().range([0, width]);
  var y = d3.scale.linear().range([height, 0]);

  var color = d3.scale.category10();

  var xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom")
      .ticks(5).tickFormat(d3.time.format("%d %m %Y"));

  var yAxis = d3.svg.axis()
      .scale(y)
      .orient("left")
      .ticks(10);

  var line = d3.svg.line()
      .interpolate("basis")
      .x(function(d) { return x(d.date); })
      .y(function(d) { return y(d.weightValue); });

  var svg = d3.select("#graph").append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  color.domain(d3.keys(data[0]).filter(function(key) { return key !== "date"; }));

  data.forEach(function(d) {
    d.date = parseDate(d.date);
  });

  var weightData = color.domain().map(function(name) {
    return {
      name: name,
      values: data.map(function(d) {
        return {date: d.date, weightValue: +d[name]};
      })
    };
  });

  x.domain(d3.extent(data, function(d) { return d.date; }));
  y.domain([
   d3.min(weightData, function(c) { return d3.min(c.values, function(v) { return v.weightValue; }); }) - 20,
   d3.max(weightData, function(c) { return d3.max(c.values, function(v) { return v.weightValue; }); }) + 20
 ]);

  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("Masa (kg)");

  var weightElement = svg.selectAll(".weight")
      .data(weightData)
    .enter().append("g")
      .attr("class", "weight");

  weightElement.append("path")
      .attr("class", "line")
      .attr("d", function(d) { return line(d.values); })
      .style("stroke", function(d) { return color(d.name); });

  weightElement.append("text")
      .datum(function(d) { return {name: d.name, value: d.values[d.values.length - 1]}; })
      .attr("transform", function(d) { return "translate(" + x(d.value.date) + "," + y(d.value.weightValue) + ")"; })
      .attr("x", 10)
      .attr("dy", ".35em")
      .text(function(d) { return d.name; });
}

/*
*
* --------------------- Google MAPS API ---------------------
*
*/

var map;
var infowindow;

function initMap(buildingId) {
  
  var type;
  if(buildingId == 1)
    type = ['pharmacy']
  else if(buildingId == 2)  
    type = ['food']
  else if(buildingId == 3)
    type = ['hospital']
  
  var location = {lat: 46.0518, lng: 14.5077}; // location for ljubljana

  map = new google.maps.Map(document.getElementById('map'), {
    center: location,
    zoom: 13
  });

  infowindow = new google.maps.InfoWindow();
  var service = new google.maps.places.PlacesService(map);
  service.nearbySearch({
    location: location,
    radius: 8000, // 1000 = 1km radius
    type: type // lekarne
  }, callback);
}

function callback(results, status) {
  if (status === google.maps.places.PlacesServiceStatus.OK)
    for (var i = 0; i < results.length; i++) 
      createMarker(results[i]);
}

function createMarker(place) {
  var marker = new google.maps.Marker({
    map: map,
    position: place.geometry.location
  });

  google.maps.event.addListener(marker, 'click', function() {
    infowindow.setContent(place.name);
    infowindow.open(map, this);
  });
}

/* global $ */